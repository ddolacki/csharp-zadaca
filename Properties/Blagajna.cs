using System;
using System.Collections.Generic;

namespace CSharp_vjezbe.Properties

{
    public class Blagajna
    {
        public Blagajna()
        {
            blagajna = new List<IRacun>();
        }

        public void DodajRacun(IRacun racun)
        {
            blagajna.Add(racun);
        }

        public void PrikaziRacune()
        {
            foreach (var racun in blagajna)
            {
                Console.WriteLine(racun.DohvatiIznos());
            }
            
        }
        
       private List<IRacun> blagajna;
    }
}