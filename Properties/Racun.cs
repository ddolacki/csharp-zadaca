using System;

namespace CSharp_vjezbe.Properties
{
    public class Racun<T> : IRacun
    {
        public Racun(T iznosRacuna)
        {
            IznosRacuna = iznosRacuna;
            DatumIzdavanja=DateTime.Now;
        }

        public decimal DohvatiIznos()
        {
            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime DohvatiDatumIzdavanja()
        {
            return DatumIzdavanja;
        }

        public T IznosRacuna
        {

            get { return iznosRacuna; }

            set
            {

                if (Convert.ToInt32(value) < 10)
                {
                    throw new IznimkaBlagajne("Premali iznos");
                }
                iznosRacuna = value;
            }
        }

        private T iznosRacuna;
        
        public DateTime DatumIzdavanja { get; private set; }

    }
}