using System;

namespace CSharp_vjezbe.Properties
{
    public interface IRacun
    {
        decimal DohvatiIznos();
        DateTime DohvatiDatumIzdavanja();
    }
}