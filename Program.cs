﻿using CSharp_vjezbe.Properties;

namespace CSharp_vjezbe
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Blagajna blag = new Blagajna();

            Racun<int> r1 = new Racun<int>(20);
            Racun<string> r2 = new Racun<string>("20");

            blag.DodajRacun(r1);
            blag.DodajRacun(r2);
            
            blag.PrikaziRacune();
        }
    }
}